function solution(ip) {
    const re = new RegExp('(^[0-9 .]*$)')
    const arr = ip.split(".")
    if (arr.length > 4 || re.test(ip) === false) return false

    for (const numStr of arr) {
        const numInt = parseInt(numStr)
        if (numStr[0] === "0" || numStr[numStr.length - 1] === "0") return false
        if (numInt < 1 || numInt > 255) return false
    }

    return true
}

let res = solution("1.2.3.4")
console.log("\n====RESULT====")
console.log(res)