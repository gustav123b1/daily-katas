import { isValidIP } from ".";

it('should return true when given "1.2.3.4" ', () => {
    const input = "1.2.3.4"
    const output = isValidIP(input)
    expect(output).toBe(true)
})

it('should return true when given "103.45.67.89" ', () => {
    const input = "103.45.67.89"
    const output = isValidIP(input)
    expect(output).toBe(true)
})

it('should return false when given an ip with less than 4 numbers', () => {
    const input = "1.2.3"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip with more than 4 numbers', () => {
    const input = "1.2.3.4.5"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains a number that starts with a 0', () => {
    const input = "123.045.67.89"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains a number that ends with a 0', () => {
    const input = "123.456.78.90"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains a number that is greater than 255', () => {
    const input = "123.56.256.90"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains a number that is less than 1', () => {
    const input = "123.456.-5.90"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains a number that is less than 1', () => {
    const input = "123.456.0.90"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains special characters that is not a "." ', () => {
    const input = "1.2.3,4"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains special characters that is not a "." ', () => {
    const input = "1.2.3?4"
    const output = isValidIP(input)
    expect(output).toBe(false)
})

it('should return false when given an ip that contains special characters that is not a "." ', () => {
    const input = "1.2.3.4:"
    const output = isValidIP(input)
    expect(output).toBe(false)
})