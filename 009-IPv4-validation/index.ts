export function isValidIP(ip: string): boolean {
    const re = new RegExp('(^[0-9 .]*$)')
    const arr = ip.split(".")
    if (arr.length !== 4 || !re.test(ip)) return false

    for (const numStr of arr) {
        const numInt = parseInt(numStr)
        if (numStr[0] === "0" || numStr[numStr.length - 1] === "0") return false
        if (numInt < 1 || numInt > 255) return false
    }

    return true
}