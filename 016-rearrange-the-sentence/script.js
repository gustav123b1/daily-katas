function rearrange(str) {
    let arr = str.split(" ")
    let strObjects = []

    // Find the number in each word and separate it from the string
    arr.forEach(e => strObjects.push(findNumInStr(e)))

    // Sort the object by the number
    let sortedStrObjects = strObjects.sort((a, b) => a.number - b.number)

    // The output should only contain the strings
    let output = sortedStrObjects.map(e => e.str)

    // Transform the array of strings to a single string
    return output.join(" ")
}

function findNumInStr(str) {
    let arr = str.split("")
    let number
    let remaining = ""

    for (let e of arr) {
        let potentialNumber = parseInt(e)

        // String = not a number (NaN)
        if (isNaN(potentialNumber))
            remaining += e
        // Number = a number(not NaN)
        else
            number = potentialNumber
    }

    return { str: remaining, number: number }
}


let res = rearrange(" ")
console.log(res)