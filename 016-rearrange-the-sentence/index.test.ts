import { rearrange } from "."
import { findNumInStr } from "."

/*
 * 
    TEST findNumInStr FUNCTION  
 *
 */

test("should return {str:'thetext', num:5} when given 'th5etext' ", () => {
	const input = "th5etext"
	const output = findNumInStr(input)
	const expectedOutput = { str: "thetext", number: 5 }
	expect(output).toEqual(expectedOutput)
})

/* 
 *
    TEST rearrange FUNCTION 
 *
 */

test('should return "This is a Test" when given "is2 Thi1s T4est 3a" ', () => {
	const input = "is2 Thi1s T4est 3a"
	const output = rearrange(input)
	const expectedOutput = "This is a Test"
	expect(output).toBe(expectedOutput)
})

test('should return "For the good of the people" when given "4of Fo1r pe6ople g3ood th5e the2" ', () => {
	const input = "4of Fo1r pe6ople g3ood th5e the2"
	const output = rearrange(input)
	const expectedOutput = "For the good of the people"
	expect(output).toBe(expectedOutput)
})

test('should return "JavaScript is so damn weird" when given "5weird i2s JavaScri1pt dam4n so3" ', () => {
	const input = "5weird i2s JavaScri1pt dam4n so3"
	const output = rearrange(input)
	const expectedOutput = "JavaScript is so damn weird"
	expect(output).toBe(expectedOutput)
})

test('should return "" when given " " ', () => {
	const input = " "
	const output = rearrange(input)
	const expectedOutput = ""
	expect(output).toBe(expectedOutput)
})
