export function rearrange(str: string) {
	if (str === " " || !str) return ""

	let arr = str.split(" ")
	let strObjects: any[] = []

	// Find the number in each word and separate it from the string
	arr.forEach((e: string) => strObjects.push(findNumInStr(e)))

	// Sort the object by the number
	let sortedStrObjects = strObjects.sort((a, b) => a.number - b.number)

	// The output should only contain the strings
	let output = sortedStrObjects.map((e) => e.str)

	// Transform the array of strings to a single string
	return output.join(" ")
}

export function findNumInStr(str: string) {
	let arr = str.split("")
	let number: number
	let remainingStr = ""

	for (let e of arr) {
		let potentialNumber = parseInt(e)

		// String = not a number (NaN)
		if (isNaN(potentialNumber)) remainingStr += e
		// Number = a number(not NaN)
		else number = potentialNumber
	}

	return { str: remainingStr, number: number! }
}
