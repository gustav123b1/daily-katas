function maxPossible(num1, num2) {
    const firstDigits = num1.toString().split("").map(str => parseInt(str))
    const swappables = num2.toString().split("").map(str => parseInt(str)).sort().reverse()

    let output = []
    for (let [i, digit] of firstDigits.entries()) {
        let swappable = swappables[0]
        let largest = swappable > digit ? swappables.shift() : digit
        output.push(largest)
    }

    return parseInt(output.join(""))
}

let res = maxPossible(8732, 91255)
console.log(res)