import { happyAlgorithm } from "."

test("should return 'HAPPY 5' when given 139 ", () => {
	const input = 139
	const output = happyAlgorithm(input)
	expect(output).toBe("HAPPY 5")
})

test("should return 'HAPPY 1' when given 1 ", () => {
	const input = 1
	const output = happyAlgorithm(input)
	expect(output).toBe("HAPPY 1")
})

test("should return 'SAD 8' when given 89 ", () => {
	const input = 89
	const output = happyAlgorithm(input)
	expect(output).toBe("SAD 8")
})

test("should return 'SAD 10' when given 67 ", () => {
	const input = 67
	const output = happyAlgorithm(input)
	expect(output).toBe("SAD 10")
})
