function happyAlgorithm(num) {
    let step = 1
    let transform = num
    let results = [num]

    while (true) {
        let transformedValue = 0
        let str = `${transform}`

        for (let i = 0; i < str.length; i++) {
            transformedValue += parseInt(str[i]) * parseInt(str[i])
        }

        transform = transformedValue

        if (transformedValue === 1) return `Happy ${step}`
        else if (results.includes(transformedValue)) return `Sad ${step}`

        results.push(transformedValue)
        step++
    }
}


const res = happyAlgorithm(89)
console.log(res)