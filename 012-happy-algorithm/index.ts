export function happyAlgorithm(num: number) {
	let step = 1
	let transform = num
	let results = [num]

	while (true) {
		let transformedValue = 0
		let str = `${transform}`

		// Squaring digits
		for (let i = 0; i < str.length; i++) {
			transformedValue += parseInt(str[i]) * parseInt(str[i])
		}

		// Update transform variable so squaring digits will be
		// applied to the transformed value in the next iteration
		transform = transformedValue

		// 1 = Happy number :)
		if (transformedValue === 1) return `HAPPY ${step}`
		// If transformed value has been reached before = infinite loop
		else if (results.includes(transformedValue)) return `SAD ${step}`

		// Add the transformed value so it can be used to check for infinite loops
		results.push(transformedValue)
		step++
	}
}
