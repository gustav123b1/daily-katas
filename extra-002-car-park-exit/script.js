function parking_exit(arr) {
    arr = getRemainingFloors(arr)

    const route = []
    let i = 0
    let stairsCount = 0
    let currentStairsIndex

    while (true) {
        let floor = arr[i]
        let stairsIndex = floor.indexOf(1)
        let userIndex = floor.indexOf(2)

        // If the user is not on the floor it means it is currently walking the stairs
        if (userIndex === -1) {
            // If the same stairs are more than 1 floor long, the player should keep walking
            if (stairsIndex === currentStairsIndex)
                stairsCount++
            // If the player has completed walking down the stairs he should be on the floor where the stairs end
            else {
                route.push(`D${stairsCount}`)
                userIndex = currentStairsIndex
                stairsCount = 0
            }
        }
        // The player is currently not walking down the stairs, but moving towards the stairs
        else if (stairsIndex !== -1) {
            // Move to the stairs
            route.push(getPath(userIndex, stairsIndex))
            // Walk down the stairs
            currentStairsIndex = stairsIndex
            stairsCount++
        }

        // At the bottom floor
        if (stairsIndex === -1) {
            route.push(getPath(userIndex, floor.length - 1))
            break
        }

        i++
    }
    if (route[0] === undefined) return []
    return route
}

function getPath(userIndex, targetIndex) {
    let diff = targetIndex - userIndex
    if (diff === 0) return
    return diff > 0 ? `R${Math.abs(diff)}` : `L${Math.abs(diff)}`
}

function getRemainingFloors(arr) {
    let playerFound = false
    return arr.filter(floor => {
        if (floor.includes(2)) playerFound = true
        return playerFound
    })
}


let res = parking_exit([[0, 0, 0, 0, 2]])
console.log(res)