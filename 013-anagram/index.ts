export function anagrams(word: string, arr: string[]) {
	// If either input is missing throw an error
	if (word === undefined || arr === undefined)
		throw new Error("Input missing")

	// The input word represents the "correct" char frequency, the words in the input array will be compared to this.
	const correctCharCount = getCharFreq(word)
	const result: string[] = []

	for (const w of arr) {
		const charCount = getCharFreq(w)
		if (isEqual(charCount, correctCharCount)) result.push(w)
	}

	return result
}

// Return the character frequency in a word
function getCharFreq(word: string) {
	const charCount = {}

	// Undefined means it is the first time a char is occuring in a word, which means the char count is 1
	word.split("").forEach((char) => {
		if (charCount[char] === undefined) charCount[char] = 1
		else charCount[char]++
	})

	return charCount
}

// Check if two objects has the same keys and values
// sort() is used to for example [b, a] -> [a, b] since "[a, b]" !== "[b, a]"
function isEqual(ob1: object, ob2: object) {
	return (
		JSON.stringify(Object.entries(ob1).sort()) ==
		JSON.stringify(Object.entries(ob2).sort())
	)
}
