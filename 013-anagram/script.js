function anagrams(word, arr) {
    const correctWordCount = getAnagramWordCount(word)
    const result = []

    for (const w of arr) {
        const anagramWordCount = getAnagramWordCount(w)
        if (isEqual(anagramWordCount, correctWordCount))
            result.push(w)
    }

    return result
}

function getAnagramWordCount(word) {
    console.log("")
    let test = {}
    word.split("").forEach(char => {
        if (test[char] === undefined)
            test[char] = 1
        else
            test[char]++
    })
    console.log(test)

    const anagramWordCount = {}
    for (let i = 0; i < word.length; i++) {
        const char = word[i]
        if (anagramWordCount[char] === undefined)
            anagramWordCount[char] = 1
        else
            anagramWordCount[char]++
    }
    console.log(anagramWordCount)
    return anagramWordCount
}

function isEqual(ob1, ob2) {
    return JSON.stringify(Object.entries(ob1).sort()) == JSON.stringify(Object.entries(ob2).sort())
}

const res = anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer'])
console.log(res)