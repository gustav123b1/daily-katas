import { anagrams } from "."

test("should return ['aabb', 'bbaa'] when given 'abba', ['aabb', 'abcd', 'bbaa', 'dada']", () => {
	const anagram = "abba"
	const arr = ["aabb", "abcd", "bbaa", "dada"]
	const output = anagrams(anagram, arr)
	expect(output).toStrictEqual(["aabb", "bbaa"])
})

test("should return ['carer', 'racer'] when given 'racer',['crazer', 'carer', 'racar', 'caers', 'racer']", () => {
	const anagram = "racer"
	const arr = ["crazer", "carer", "racar", "caers", "racer"]
	const output = anagrams(anagram, arr)
	expect(output).toStrictEqual(["carer", "racer"])
})

test("should return [] when given 'laser',['lazing', 'lazy',  'lacer']", () => {
	const anagram = "laser"
	const arr = ["lazing", "lazy", "lacer"]
	const output = anagrams(anagram, arr)
	expect(output).toStrictEqual([])
})

test("should throw an Error when only providing an anagram without an array", () => {
	const anagram = "anagram"
	const output = () => {
		anagrams(anagram)
	}
	expect(output).toThrow(Error)
})

test("should throw an Error when inputs are missing", () => {
	const output = () => {
		anagrams()
	}
	expect(output).toThrow(Error)
})
