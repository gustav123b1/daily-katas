function clockwiseCipher(message) {
    const sideLen = Math.ceil(Math.sqrt(message.length))
    let square = [...Array(sideLen)].map(e => Array(sideLen));


    let currentLayer = 0
    let innerSideLen = sideLen - 0 * 2
    let directions = ["right", "down", "left", "up",]
    // Direction where the steps will be taken
    let direction = directions[0]
    let curLoop = 0
    //let currentIndex = 0

    // Current cycle.
    for (let c = 0; c < sideLen; c++) {

        if (c >= innerSideLen - 1) {
            currentLayer++
            innerSideLen = sideLen - currentLayer * 2
        }
        let currentCol = c % (innerSideLen - 1) + currentLayer
        let currentRow = currentLayer


        let currentIndex = getIndexFromRowCol(sideLen, currentRow, currentCol)


        // Current rotation in cycle
        for (let j = 0; j < 4; j++) {
            // Where the letter will be placed in the square

            // Current letter
            let letter = message[curLoop] === undefined ? " " : message[curLoop]
            console.log(letter)

            // Amount of steps required to reach destination
            let steps = innerSideLen - 1

            // Direction where the steps will be taken
            direction = directions[curLoop % 4]

            let newIndex = getIndexAfterStep(sideLen, innerSideLen, currentIndex, direction, steps, currentLayer)
            let pos = getRowColFromIndex(sideLen, currentIndex)


            // console.log(direction, pos, letter, currentIndex)
            //console.log(letter, currentIndex, direction)
            //console.log(currentIndex, direction, newIndex, pos)

            //console.log(pos, letter)
            square[pos.row][pos.col] = letter

            currentIndex = newIndex

            // How many times it has been looped in total
            curLoop++
        }
    }
    console.log(square)
    return square.map(row => row.join("")).join("")
}


function getIndexAfterStep(sqSize, innerSqSize, index, dir, steps, layer) {
    if (dir === "right") {
        let { row, col } = getRowColFromIndex(sqSize, index)
        col += steps

        // Check for overflow in matrix, stepping outside matrix
        if (col >= innerSqSize + layer) {
            let overflow = Math.abs(innerSqSize - col - 1 + layer)
            let edge = getIndexFromRowCol(sqSize, row, innerSqSize - 1 + layer)
            return getIndexAfterStep(sqSize, innerSqSize, edge, "down", overflow, layer)
        }
        return getIndexFromRowCol(sqSize, row, col)
    }
    else if (dir === "down") {
        let { row, col } = getRowColFromIndex(sqSize, index)
        row += steps

        // Check for overflow in matrix, stepping outside matrix
        if (row >= innerSqSize + layer) {
            let overflow = Math.abs(innerSqSize - row - 1 + layer)
            let edge = getIndexFromRowCol(sqSize, innerSqSize - 1 + layer, col)
            return getIndexAfterStep(sqSize, innerSqSize, edge, "left", overflow, layer)
        }
        return getIndexFromRowCol(sqSize, row, col)
    }
    else if (dir === "left") {
        let { row, col } = getRowColFromIndex(sqSize, index)
        col -= steps

        // Check for overflow in matrix, stepping outside matrix
        if (col < layer) {
            let edge = getIndexFromRowCol(sqSize, row, layer)
            let overflow = Math.abs(col)
            return getIndexAfterStep(sqSize, innerSqSize, edge, "up", overflow, layer)
        }
        return getIndexFromRowCol(sqSize, row, col)
    }
    else if (dir === "up") {
        let { row, col } = getRowColFromIndex(sqSize, index)
        row -= steps

        // Check for overflow in matrix, stepping outside matrix
        if (row < layer) {
            let edge = getIndexFromRowCol(sqSize, layer, col)
            let overflow = Math.abs(row)
            return getIndexAfterStep(sqSize, innerSqSize, edge, "right", overflow, layer)
        }
        return getIndexFromRowCol(sqSize, row, col)
    }
}

// Asuming its a square
function getIndexFromRowCol(sqSize, row, col) {
    return row * sqSize + col
}

function getRowColFromIndex(sqSize, i) {
    let row = Math.floor(i / sqSize)
    let col = i - row * sqSize
    return { row: row, col: col }
}


/* let res = clockwiseCipher("Mubashir Hassan")
console.log(res)
console.log(clockwiseCipher("Matt MacPherson")) */


console.log(clockwiseCipher("Mubashir Hassan"))
/* console.log(clockwiseCipher("Edabit is amazing")) */