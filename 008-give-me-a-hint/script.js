function grantTheHint(str) {
    const arr = str.split(" ")
    const hiddenArr = getEmpty(arr)
    const result = [getEmpty(arr).join(" ")]
    const longestWord = arr.reduce((a, b) => { return a.length > b.length ? a : b; })

    for (let i = 0; i < longestWord.length; i++) {
        for (let [j, word] of arr.entries()) {
            if (word.length < i) continue
            const newWord = hiddenArr[j].split("")
            newWord[i] = word[i]
            hiddenArr[j] = newWord.join("")
        }
        result.push(hiddenArr.join(" "))
    }
    console.log(result)
    return result
}

function getEmpty(arr) {
    return arr.map(w => { return "_".repeat(w.length) })
}

grantTheHint("Mary Queen of Scots")