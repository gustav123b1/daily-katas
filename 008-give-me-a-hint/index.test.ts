import { grantTheHint } from ".";

it(`it should return
    ["____ _____ __ _____", 
    "M___ Q____ o_ S____", 
    "Ma__ Qu___ of Sc___", 
    "Mar_ Que__ of Sco__", 
    "Mary Quee_ of Scot_", 
    "Mary Queen of Scots"]  
    when given "Mary Queen of Scots"`,
    () => {
        const input = "Mary Queen of Scots"
        const output = grantTheHint(input)
        expect(output).toStrictEqual([
            "____ _____ __ _____",
            "M___ Q____ o_ S____",
            "Ma__ Qu___ of Sc___",
            "Mar_ Que__ of Sco__",
            "Mary Quee_ of Scot_",
            "Mary Queen of Scots"
        ])
    })

it(`it should return
    ["___ ____ __ __", 
    "T__ L___ o_ P_", 
    "Th_ Li__ of Pi", 
    "The Lif_ of Pi", 
    "The Life of Pi"]  
    when given "The Life of Pi"`,
    () => {
        const input = "The Life of Pi"
        const output = grantTheHint(input)
        expect(output).toStrictEqual([
            "___ ____ __ __",
            "T__ L___ o_ P_",
            "Th_ Li__ of Pi",
            "The Lif_ of Pi",
            "The Life of Pi"
        ])
    })