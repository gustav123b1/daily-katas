export function charCount(char, str) {
    let charFreq = 0
    if (char.length > 1) {
        throw new Error("Char cannot contain more than 2 characters")
    }

    for (let i = 0; i < str.length; i++) {
        let strChar = str[i]

        if (char == strChar) {
            charFreq++
        }
    }
    return charFreq
}