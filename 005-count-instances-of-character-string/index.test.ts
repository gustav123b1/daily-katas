import { charCount } from "."

it("it should return 1", () => {
    let count = charCount("a", "edabit")
    expect(count).toBe(1)
})

it("it not count if char is lowercase and the letter in string is uppercase", () => {
    let count = charCount("c", "Chamber of secrets")
    expect(count).toBe(1)
})

it("it not count if char is uppercase and the letter in string is lowercase", () => {
    let count = charCount("B", "boxes are fun")
    expect(count).toBe(0)
})

it("it should work with every character", () => {
    let count = charCount("!", "!easy!")
    expect(count).toBe(2)
})

it("it should throw an error is char is more than 1 character", () => {

    const count = () => {
        charCount("wow", "the universe is wow")
    }
    expect(count).toThrow(Error);
})

