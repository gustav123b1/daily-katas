import { correctTitle } from "."

it("should end with a '.'", () => {
    let title = correctTitle("jOn SnoW, kINg IN thE noRth")
    let lastChar = title[title.length - 1]
    expect(lastChar).toBe(".")
})

it("should not capitalize 'and'", () => {
    let title = correctTitle("test and asdasdasd.")
    expect(title.includes("And")).toBe(false)
})

it("should lowercase 'And'", () => {
    let title = correctTitle("test And asdasdasd.")
    expect(title.includes("and")).toBe(true)
})

it("should give expected output 'Jon Snow, King in the North.'", () => {
    let title = correctTitle("jOn SnoW, kINg IN thE noRth")
    expect(title).toBe("Jon Snow, King in the North.")
})

it("should add a space after every comma", () => {
    let title = correctTitle("Asd3,Asd2,Asd3 Test,Test2.")
    expect(title).toBe("Asd3, Asd2, Asd3 Test, Test2.")
})

it("should capitalize first character and lowercase the rest", () => {
    let title = correctTitle("aSD ASD Asd.")
    expect(title).toBe("Asd Asd Asd.")
})

it("should seperate words that has -", () => {
    let title = correctTitle("test-asd")
    expect(title).toBe("Test-Asd.")
})