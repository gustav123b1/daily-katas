export function correctTitle(str) {
    str = str.replaceAll(",", ", ")

    let splitArr = str.split(" ")
    let result = ""

    for (let [i, str] of splitArr.entries()) {
        if (str == "") continue
        let word = formatWord(str, splitArr, i)
        result += word
    }
    return result
}

function formatWord(word, arr, i) {
    let wordEnd
    if (i == arr.length - 1) {
        let lastChar = word[word.length - 1]
        if (lastChar == ".") wordEnd = ""
        else wordEnd = "."
    }
    else
        wordEnd = " "

    if (word.includes("-")) {
        let arr = word.split("-")
        return `${giveCorrectCase(arr[0], true)}-${giveCorrectCase(arr[1])}`
    }
    else
        return giveCorrectCase(word)

    function giveCorrectCase(word, hyph = false) {
        let lowerCaseWord = word.toLowerCase()
        let end = (hyph) ? "" : wordEnd
        if (lowerCaseWord == "and" || lowerCaseWord == "the" || lowerCaseWord == "of" || lowerCaseWord == "in")
            return word.toLowerCase() + end
        return `${word.toUpperCase()[0]}${lowerCaseWord.slice(1)}${end}`
    }
}