function _correctTitle(str) {
    str = str.replace(",", ", ")
    console.log("asd,assad,asdasd".replace(",", " === "))
    let splitArr = str.split(" ")
    let result = ""
    console.log(splitArr)

    for (let [i, str] of splitArr.entries()) {
        if (str == "") continue
        let word = _formatWord(str, splitArr, i)
        console.log(word)
        result += word
    }
    return result
}

function _formatWord(word, arr, i) {
    let lowerCaseWord = word.toLowerCase()
    let wordEnd = (i == arr.length - 1) ? "." : " "
    if (i == arr.length - 1) {
        let lastChar = word[word.length - 1]
        if (lastChar == ".") wordEnd = ""
    }

    if (lowerCaseWord == "and" || lowerCaseWord == "the" || lowerCaseWord == "of" || lowerCaseWord == "in")
        return word.toLowerCase() + wordEnd
    return `${word.toUpperCase()[0]}${lowerCaseWord.slice(1)}${wordEnd}`
}

console.log(_correctTitle("Asd3,Asd2,Asd3 Test,Test2"))
