export function secondLargest(arr) {
    if (arr.length === 1)
        return arr[0]
    if (arr.length === 0 || (arr.length === 2 && arr[0] === arr[1]))
        return 0

    const numbers = {}
    arr.forEach(num => numbers[num] = true)

    return Object.keys(numbers).length === 1 ?
        parseInt(Object.keys(numbers)[0]) : parseInt(Object.keys(numbers).reverse()[1])
}

