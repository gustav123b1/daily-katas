function _secondLargest(arr) {
    if (arr.length === 1) return arr[0]
    if (arr.length === 0) return 0
    if (arr.length === 2 && arr[0] == arr[1]) return 0

    const numbers = {}
    arr.forEach(num => numbers[num] = true)
    return Object.keys(numbers).length === 1 ? parseInt(Object.keys(numbers)[0]) : parseInt(Object.keys(numbers).reverse()[1])
}

let res = _secondLargest([1, 1])
console.log(res)