import { secondLargest } from "."

it("should return 40 when given [10, 40, 30, 20, 50]", () => {
    const input = [10, 40, 30, 20, 50]
    const output = secondLargest(input)
    expect(output).toBe(40)
})

it("should return 23 when given [54, 23, 11, 17, 10]", () => {
    const input = [54, 23, 11, 17, 10]
    const output = secondLargest(input)
    expect(output).toBe(23)
})

it("should return 70 when given [100, 50, 40, 60, 70]", () => {
    const input = [100, 50, 40, 60, 70]
    const output = secondLargest(input)
    expect(output).toBe(70)
})

it("should return 105 when given [25, 143, 89, 13, 105]", () => {
    const input = [25, 143, 89, 13, 105]
    const output = secondLargest(input)
    expect(output).toBe(105)
})

it("should return 5 when given [10, 10, 10, 5, 4, 5]", () => {
    const input = [10, 10, 10, 5, 4, 5]
    const output = secondLargest(input)
    expect(output).toBe(5)
})

it("should return 10 when given [10, 10, 10]", () => {
    const input = [10, 10, 10]
    const output = secondLargest(input)
    expect(output).toBe(10)
})

it("should return 0 when given an array only has two numbers that are equal", () => {
    const input = [1, 1]
    const output = secondLargest(input)
    expect(output).toBe(0)
})

it("should return 0 when given an empty array ", () => {
    const input = []
    const output = secondLargest(input)
    expect(output).toBe(0)
})

it("should return the only element when given an array with one element", () => {
    const input = [1]
    const output = secondLargest(input)
    expect(output).toBe(1)
})