export function shuffleCount(
	num: number,
	deck?: number[],
	original?: number[],
	count: number = 0
) {
	// On the first run the deck is undefined
	// If undefined generate a new deck from 1..N
	if (deck === undefined) {
		deck = [...Array(num + 1).keys()]
		deck.shift()
		original = deck
	}
	if (isEven(deck.length) === false)
		throw new Error("Deck must contain an equal amount of cards")

	// Split the deck into two equal halves
	const [firstHalf, secondHalf] = [
		[...deck.slice(0, deck.length / 2)],
		[...deck.slice(deck.length / 2)],
	]

	// Loop through the first half
	firstHalf.forEach((card, i) => {
		// Take the top card from the other half and place at index (i * 2 + 1) in the first half
		// Adding +1 at the end makes sure that the card doesnt get placed on top of the top card in the first half
		firstHalf.splice(i * 2 + 1, 0, secondHalf.shift()!)
	})

	count++

	if (arrayEqualCheck(firstHalf, original!)) return count
	return shuffleCount(num, firstHalf, original, count)
}

function isEven(n: number) {
	return n % 2 === 0
}

function arrayEqualCheck(arr1: any[], arr2: any[]) {
	return JSON.stringify(arr1) == JSON.stringify(arr2)
}
