import { shuffleCount } from "."

test("should return 3 when given 8", () => {
	const input = 8
	const output = shuffleCount(input)
	const expectedOutput = 3

	expect(output).toBe(expectedOutput)
})

test("should return 12 when given 14", () => {
	const input = 14
	const output = shuffleCount(input)
	const expectedOutput = 12

	expect(output).toBe(expectedOutput)
})

test("should return 8 when given 52", () => {
	const input = 52
	const output = shuffleCount(input)
	const expectedOutput = 8

	expect(output).toBe(expectedOutput)
})

test("should should throw an error when given an odd number", () => {
	const input = 13
	const output = () => shuffleCount(input)
	const expectedOutput = Error

	expect(output).toThrow(expectedOutput)
})
