function shuffleCount(num, deck, original, count = 0) {
    if (deck === undefined) {
        deck = [...Array(num + 1).keys()]
        deck.splice(0, 1)
        original = deck
    }
    if (isEven(deck.length) === false)
        throw new Error("Deck must contain an equal amount of cards")

    // First split the deck into two halves
    const [firstHalf, secondHalf] = [[...deck.slice(0, deck.length / 2)], [...deck.slice(deck.length / 2)]]

    // Loop through the first half
    firstHalf.forEach((card, i) => {
        // Take the top card from the other half and place at index (i * 2 + 1) in the first half
        // This makes sure it doesnt get placed on top of the top card in the first half
        firstHalf.splice(i * 2 + 1, 0, secondHalf.shift())
    })

    count++
    if (arraysEqual(firstHalf, original) === false) {
        return shuffleCount(num, firstHalf, original, count)
    }
    return count
}

function isEven(n) {
    return (n % 2 === 0)
}

function arraysEqual(a1, a2) {
    return JSON.stringify(a1) == JSON.stringify(a2);
}


console.log(shuffleCount(16))

