import { XAndO } from "."

test('should return [1, 3] when given [" | | ", " |X| ", "X| | "]', () => {
	const input = [" | | ", " |X| ", "X| | "]
	const output = XAndO(input)
	expect(output).toEqual([1, 3])
})

test('should return [3, 3] when given ["X|X|O", "O|X| ", "X|O| "]', () => {
	const input = ["X|X|O", "O|X| ", "X|O| "]
	const output = XAndO(input)
	expect(output).toEqual([3, 3])
})

test("should return false when given a board where there is no win", () => {
	const input = ["X|X|O", "O|X| ", "X|O|O"]
	const output = XAndO(input)
	expect(output).toBe(false)
})
