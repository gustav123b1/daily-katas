export function XAndO(b: string[]) {
	const boardSize = 3
	// Split the array to a 2d array which represents the board
	const board = b.map((row: string) => row.split("|"))

	// Get all the lines
	const lines = [...getStraightLines(), ...getDiagnalLines()]

	// Loop through the lines and see if the symbols on the board matches a line
	for (let line of lines) {
		// A winning line has 1 empty square and 2 "X" squares
		let emptyCount = 0
		let xCount = 0
        
		// Position of the empty square in winning like
		let emptyRowCol: any = {}

		// Loop through the squares in the line, each square represents an index on the board
		for (let lineSuqareIndex of line) {
			// Get the square on the board which has the same index as the square in the line
			let boardSquare = getSuqaureOfIndex(
				lineSuqareIndex,
				board,
				boardSize
			)

			// If square contains anything that is not an X or empty it cannot be a winning line.
			if (boardSquare !== " " && boardSquare !== "X") break

			if (boardSquare === " ") {
				emptyCount++
				emptyRowCol = getRowColByIndex(lineSuqareIndex, boardSize)
			} else if (boardSquare === "X") xCount++

			// If line contains more than 1 empty square its not winning.
			if (emptyCount > 1) break

			// If line contains 2 X and 1 empty the line is winning
			if (emptyCount === 1 && xCount === 2) {
				return [emptyRowCol.row + 1, emptyRowCol.col + 1]
			}
		}
	}
    
	return false
}

// Get the row and column by index
function getRowColByIndex(i: number, boardSize = 3) {
	let row = Math.floor(i / 3)
	let col = i - row * boardSize
	return { row: row, col: col }
}

// Get a square on the board by index
function getSuqaureOfIndex(i: number, board: string[][], boardSize = 3) {
	let { row, col } = getRowColByIndex(i, boardSize)
	return board[row][col]
}

// Get vertical and horizontal lines
function getStraightLines(boardSize: number = 3) {
	let res: number[][] = []
	for (let row = 0; row < boardSize; row++) {
		let h: number[] = []
		let v: number[] = []
		for (let col = 0; col < boardSize; col++) {
			h.push(row * boardSize + col)
			v.push(col * boardSize + row)
		}
		res.push(h, v)
	}

	return res
}

// Get diagonal lines (Doesnt really work for boards larger than 3x3 but who cares)
function getDiagnalLines(boardSize = 3) {
	let diag1: number[] = []
	let diag2: number[] = []
	for (let i = 0; i < boardSize; i++) {
		diag1.push(i + i * boardSize)
		diag2.push(boardSize - i - 1 + i * boardSize)
	}

	return [diag1, diag2]
}
