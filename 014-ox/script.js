function XAndO(b) {
    const boardSize = 3
    // Split the string to an array
    const board = b.map(row => row.split("|"))

    // Get all the lines
    const lines = [...getStraightLines(), ...getDiagnalLines()]

    console.log(board)
    // Loop through the lines and see if the symbol matches a line
    for (let line of lines) {
        let emptyCount = 0
        let xCount = 0
        let emptyRowCol
        console.log("\nNEW LINE\n")
        console.log(line)
        // Loop through the squares in a line, each square represents an index on the board
        for (let lineSuqareIndex of line) {
            // Get the on the board that is the same as the square in the line
            let boardSquare = getSuqaureOfIndex(lineSuqareIndex, board)

            // If square contains anything that is not an X or empty it cannot be a winning line.
            if (boardSquare !== " " && boardSquare !== "X") {
                console.log("BAD SQUARE")
                break
            }
            if (boardSquare === " ") {
                console.log(`EMPTY COUNT: ${emptyCount} -> ${emptyCount + 1}`)
                emptyCount++
                emptyRowCol = getRowColByIndex(lineSuqareIndex)
            }
            if (boardSquare === "X") {
                console.log(`X COUNT: ${xCount} -> ${xCount + 1}`)
                xCount++
            }

            // If line contains more than 1 empty square its not winning.
            if (emptyCount > 1) break

            // If line contains 2 X and 1 empty the line is winning
            if (emptyCount === 1 && xCount === 2) {
                return [emptyRowCol.col + 1, emptyRowCol.row + 1]
            }
        }
    }
}

function asd() {
    for (let [rowi, row] of board.entries()) {
        for (let [coli, square] of row.entries()) {
            if (square !== " " && square !== "X") continue
            // Get squares index 
            const squareIndex = rowi * boardSize + coli
            console.log(squareIndex)
        }
    }
}

function getRowColByIndex(i, board, boardSize = 3) {
    let row = Math.floor(i / 3)
    let col = i - row * boardSize
    return { row: row, col: col }
}

function getSuqaureOfIndex(i, board, boardSize = 3) {
    let { row, col } = getRowColByIndex(i, board)
    return board[row][col]
}

function getStraightLines(boardSize = 3) {
    let res = []
    for (let row = 0; row < boardSize; row++) {
        let h = []
        let v = []
        for (let col = 0; col < boardSize; col++) {
            h.push(row * boardSize + col)
            v.push(col * boardSize + row)
        }
        res.push(h, v)
    }

    return res
}

function getDiagnalLines(boardSize = 3) {
    let diag1 = []
    let diag2 = []
    for (let i = 0; i < boardSize; i++) {
        diag1.push(i + i * boardSize)
        diag2.push(boardSize - i - 1 + i * boardSize)
    }
    return [diag1, diag2]
}


let res = XAndO(["X|X|O", "O|X| ", "X|O| "])
console.log("\n=== ANSWER ===")
console.log(res)