import { elastic } from "."

it("should should return 'ANNNNA' when given 'ANNA'", () => {
    const input = "ANNA"
    const output = elastic(input)
    expect(output).toBe("ANNNNA")
})

it("should should return 'KAAYYYAAK' when given 'KAYAK'", () => {
    const input = "KAYAK"
    const output = elastic(input)
    expect(output).toBe("KAAYYYAAK")
})

it("it should return '1' if '1' is given", () => {
    const input = "1"
    const output = elastic(input)
    expect(output).toBe("1")
})

it("it should return '12' if '12' is given", () => {
    const input = "12"
    const output = elastic(input)
    expect(output).toBe("12")
})