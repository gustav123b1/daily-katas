function elastic(word) {
    const wordParts = {}
    let result = ""
    const center = Math.floor(word.length / 2)

    wordParts["Left"] = word.slice(0, center)
    if (isEven(word.length)) {
        wordParts["Right"] = word.slice(center)
    }
    else {
        wordParts["Center"] = word[center]
        wordParts["Right"] = word.slice(center + 1)
    }
    console.log(wordParts)

    let curMult = 1
    let dir = 1
    let i = 0
    for (let key in wordParts) {
        const wordPartValue = wordParts[key]
        let elasticWord = ""

        for (let j = 0; j < wordPartValue.length; j++) {
            if (isEven(word.length) && i == center - 1) dir = 0
            else if (i == center) dir = -1

            elasticWord += wordPartValue[j].repeat(curMult)
            curMult += dir
            i++
        }
        result += elasticWord
    }
    return result
}

function isEven(n) {
    return n % 2 == 0
}

elastic("KAYAK")