function PowerRanger(n, a, b) {
    let numsArr = []
    let index = 1

    while (Math.pow(index, n) <= b){
        if (Math.pow(index, n) >= a && Math.pow(index, n) <= b)
            numsArr.push(Math.pow(index, n))

        index++
    }

    return numsArr
}


let res = PowerRanger(10, 1, 5)
console.log(res)