function caesarCipher(s, k) {
    const charCodes = []

    for (let i = 0; i < s.length; i++) {
        const char = s[i]
        charCodes.push(getLetterCharcode(char, k))
    }
    return String.fromCharCode(...charCodes);
}

function getLetterCharcode(char, k) {
    let stopIndex, startIndex
    if (isCapital(char)) {
        startIndex = 0
        stopIndex = 90
    }
    else {
        startIndex = 97
        stopIndex = 122
    }
    const re = new RegExp(/([A-Za-z])/)
    if (re.test(char) === false) return char.charCodeAt(0)

    let step = char.charCodeAt(0) + k
    while (true) {
        let charStep = String.fromCharCode(step)
        if (step > stopIndex) {
            step = step - stopIndex + startIndex - 1
            charStep = String.fromCharCode(step)
            //console.log(step, "b".charCodeAt(0))
        }
        if (re.test(charStep)) {
            //console.log(charStep)
            return step
        }
        step++
    }
}

function isCapital(char) {
    return char.toUpperCase() == char
}

let res = caesarCipher("A friend in need is a friend indeed", 5)
console.log(res)