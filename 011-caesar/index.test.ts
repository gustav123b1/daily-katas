import { caesarCipher } from "."

test('should return "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj" when given "Always-Look-on-the-Bright-Side-of-Life" with a step of 5', () => {
	const output = caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5)
	expect(output).toBe("Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj")
})

test('should return "U zlcyhx ch hyyx cm u zlcyhx chxyyx" when given" "A friend in need is a friend indeed" with a step of 20', () => {
	const output = caesarCipher("A friend in need is a friend indeed", 20)
	expect(output).toBe("U zlcyhx ch hyyx cm u zlcyhx chxyyx")
})

test('should return "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj" when given "middle-Outz" with a step of 2', () => {
	const output = caesarCipher("middle-Outz", 2)
	expect(output).toBe("okffng-Qwvb")
})
