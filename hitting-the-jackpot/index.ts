export function testJackpot(outcome) {
    let firstSymbol
    for (let symbol of outcome) {
        if (firstSymbol === undefined) {
            firstSymbol = symbol
            continue
        }

        if (symbol != firstSymbol)
            return false
    }
    return true
}