import { testJackpot } from "."

it("should return true when all symbols are 1 character", () => {
    expect(testJackpot(["@", "@", "@", "@"])).toBe(true)
})

it("should return true when all symbols are more than 1 character", () => {
    expect(testJackpot(["abc", "abc", "abc", "abc"])).toBe(true)
})

it("should return false when there are different amount of characters in the symbols", () => {
    expect(testJackpot(["&&", "&", "&&&", "&&&&"])).toBe(false)
})

it("should return false if the uppercase&lowercase is not the same for all of the symbols", () => {
    expect(testJackpot(["SS", "SS", "SS", "Ss"])).toBe(false)
})