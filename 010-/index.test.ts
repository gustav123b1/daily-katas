import { howUnlucky } from "."

test("should return 2 when given 2020", () => {
	const input = 2020
	const output = howUnlucky(input)
	expect(output).toBe(2)
})

test("should return 3 when given 2026", () => {
	const input = 2026
	const output = howUnlucky(input)
	expect(output).toBe(3)
})

test("should return 1 when given 2016", () => {
	const input = 2016
	const output = howUnlucky(input)
	expect(output).toBe(1)
})

test("should throw an Error is input is not of type number", () => {
	const input = "2016"
	const output = () => howUnlucky(input)
	console.log(output)
	expect(output).toThrow(Error)
})
