export function howUnlucky(year: number) {
	if (typeof year !== "number")
		throw new Error("Provided year is not of type number.")

	const date = new Date()
	const fridayIndex = 5
	let friday13counter = 0

	for (let curMonth = 0; curMonth < 12; curMonth++) {
		date.setFullYear(year, curMonth, 13)
		if (date.getDay() == fridayIndex) friday13counter++
	}

	return friday13counter
}
