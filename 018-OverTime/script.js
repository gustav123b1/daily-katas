function OverTime(values) {
    let [start, stop, rate, mult] = values
    let { workHours, overTimeHours } = splitTimes(start, stop)
    return workHours * rate + overTimeHours * rate * mult
}

function splitTimes(start, stop) {
    let workHours = 0
    let overTimeHours = 0
    for (let hour = start; hour < stop; hour++) {
        if (hour >= 9 && hour < 17) workHours++
        else overTimeHours++
    }
    return { workHours: workHours, overTimeHours: overTimeHours }
}

let res = OverTime([16, 18, 30, 1.8])
console.log(res)